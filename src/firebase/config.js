import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'
import 'firebase/storage'


const firebaseConfig = {
    apiKey: "AIzaSyDkuX9aVI3Sf8N9S0Sst7oMR8IKwr6T--s",
    authDomain: "vue-playlist-921a8.firebaseapp.com",
    projectId: "vue-playlist-921a8",
    storageBucket: "vue-playlist-921a8.appspot.com",
    messagingSenderId: "748351943169",
    appId: "1:748351943169:web:cf9852147c42f9c42a0600"
};

// init firebase
firebase.initializeApp(firebaseConfig)

// init services
const projectFirestore = firebase.firestore()
const projectAuth = firebase.auth()
const projectStorage = firebase.storage()


// timestamp
const timestamp = firebase.firestore.FieldValue.serverTimestamp

export { projectFirestore, projectAuth, projectStorage, timestamp }